class CreateLearningMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :learning_materials do |t|
      t.string :title, null: false
      t.text :description
      t.text :attachment_url
      t.timestamps
    end
  end
end
