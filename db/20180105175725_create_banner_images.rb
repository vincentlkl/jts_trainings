class CreateBannerImages < ActiveRecord::Migration
  def change
    create_table :banner_images do |t|
      t.string :image_url
      t.integer :position, index: true
      t.timestamps
    end
  end
end
