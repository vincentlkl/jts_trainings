class ContactsController < ApplicationController

  def create

    @contact = Contact.new(params[:contact])
    @contact.request = request

    @status = true
    unless verify_recaptcha(model: @contact) && @contact.deliver
      @status = false
    end      
  end
end
