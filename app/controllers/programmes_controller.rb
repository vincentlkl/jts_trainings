class ProgrammesController < ApplicationController
  include ApplicationHelper
  
  def index
    @title = all_programmes.key(params[:id])
  end

end
