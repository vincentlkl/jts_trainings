class BannerImagesController < Admin::BaseController
  before_action :authenticate_user!, except: %I[create]

  def index
    @banner_images = BannerImage.order("position")
  end

  def create
    @banner_image = BannerImage.create(image_url: params[:url])
  end

  def destroy
    @banner_image = BannerImage.find(params[:id])
    if @banner_image.destroy
      flash[:success] = 'Deleted image.'
      redirect_to banner_images_path
    end
  end

  def move_up
    @banner_image = BannerImage.find(params[:banner_image_id])
    @banner_image.move_higher
    redirect_to banner_images_path
  end

  def move_down
    @banner_image = BannerImage.find(params[:banner_image_id])
    @banner_image.move_lower
    redirect_to banner_images_path
  end

end
