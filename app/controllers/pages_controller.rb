class PagesController < ApplicationController

  def home
    @banner_images = BannerImage.order('position').pluck(:image_url)
  end

  def about_us
  end

end
