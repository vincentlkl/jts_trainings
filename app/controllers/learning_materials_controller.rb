class LearningMaterialsController < ApplicationController
  before_action :authenticate_user!
  impressionist actions: %I[show]

  def index
    @learning_materials = LearningMaterial.paginate(page: params[:page], per_page: 10)
  end

  def show
    @learning_material = LearningMaterial.find(params[:id])
    redirect_to @learning_material.attachment_url
  end
end
