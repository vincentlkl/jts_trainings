class Admin::BaseController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin!

  private

  def authenticate_admin!
    if current_user.present? && current_user&.role != "admin"
      redirect_to root_path
    end
  end
end