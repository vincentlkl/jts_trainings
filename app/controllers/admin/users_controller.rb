class Admin::UsersController < Admin::BaseController
  before_action :set_user, only: %I[show edit update sign_in_logs]

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Created new user."
      redirect_to admin_users_path
    else
      flash[:alert] = @user.errors.full_messages.join
      render :new
    end
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = "Updated successfully."
      redirect_to admin_users_path
    else
      flash[:alert] = @user.errors.full_messages.join
      render :edit
    end
  end

  def sign_in_logs
    @sign_in_logs = @user.login_activities.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
  end

  private
  def user_params
    columns = %I[email username password password_confirmation role]
    params.require(:user)
    .permit(columns)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
