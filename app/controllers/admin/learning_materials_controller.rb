class Admin::LearningMaterialsController < Admin::BaseController
  skip_before_action :authenticate_user!, only: [:s3_upload]
  skip_before_action :authenticate_admin!, only: [:s3_upload]

  before_action :set_learning_material, only: %I[show edit update download_logs]

  def index
    respond_to do |format|
      format.html
      format.json { render json: LearningMaterialsDatatable.new(view_context) }
    end
  end

  def new
    @learning_material = LearningMaterial.new
  end

  def edit
  end

  def create
    @learning_material = LearningMaterial.new(learning_material_params)
    if @learning_material.save
      flash[:success] = "Created new Learning Material."
      redirect_to admin_learning_materials_path
    else
      flash[:alert] = @learning_material.errors.full_messages.join
      render :new
    end
  end

  def update
    if @learning_material.update_attributes(learning_material_params)
      flash[:success] = "Updated successfully."
      redirect_to admin_learning_materials_path
    else
      flash[:alert] = @learning_material.errors.full_messages.join
      render :edit
    end
  end

  def s3_upload
    @url = params.dig(:url)
  end

  def download_logs
    @logs = @learning_material.impressions.order(created_at: :desc).paginate(page: params[:page], per_page: 10)
  end

  private
  def learning_material_params
    columns = %I[title description attachment_url]
    params.require(:learning_material)
    .permit(columns)
  end

  def set_learning_material
    @learning_material = LearningMaterial.find(params[:id])
  end
end
