$ ->
  $("#S3-uploader").S3Uploader()
  $('.carousel').carousel()

  $('.ajax-datatable').dataTable
    sPaginationType: "full_numbers"
    bProcessing: true
    bServerSide: true
    lengthMenu: [[10, 25, 50, 100, 10000], [10, 25, 50, 100, 10000]]
    sAjaxSource: $('.ajax-datatable').data('source')