module ApplicationHelper
  def all_programmes
    programmes = {
      "Engineering"                  => "engineering",
      "Human Resources"              => "human-resources",
      "Finance & Accounting"         => "finance-accounting",
      "Sales & Marketing"            => "sales-marketing",
      "Management"                   => "management",
      "Soft Skills"                  => "soft-skills",
      "Administrative"               => "administrative",
      "Occupation Safety and Health" => "occupation-safety-and-health",
      "Quality & Productivity"       => "quality-productivity",
      "Logistics"                    => "logistics",
      "Information Technology"       => "information-technology"
    }
    return programmes
  end
end
