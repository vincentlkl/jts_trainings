class Contact < MailForm::Base
  attribute :name,      :validate => true
  attribute :company_name
  attribute :contact
  attribute :email,     :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :message

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => "Website Enquiry",
      :to => "info@jtstrainings.com",
      :from => %("#{name}" <#{email}>),
      :reply_to => %("#{name}" <#{email}>)
    }
  end
end
