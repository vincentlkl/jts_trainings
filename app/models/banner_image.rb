# == Schema Information
#
# Table name: banner_images
#
#  id         :integer          not null, primary key
#  image_url  :string
#  position   :integer
#  created_at :datetime
#  updated_at :datetime
#

class BannerImage < ActiveRecord::Base
  acts_as_list

end
