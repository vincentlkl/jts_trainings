class LearningMaterial < ApplicationRecord
  is_impressionable

  validates :title, presence: true
  validates :description, presence: true
  validates :attachment_url, presence: true
end
