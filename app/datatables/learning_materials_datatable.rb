class LearningMaterialsDatatable
  delegate :params, :h, :link_to, :edit_admin_learning_material_path, :download_logs_admin_learning_material_path, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: LearningMaterial.count,
      iTotalDisplayRecords: learning_materials.total_entries,
      aaData: data
    }
  end

private

  def data
    learning_materials.map do |learning_material|
      [
        learning_material.id,
        learning_material.title,
        "#{link_to("Edit", edit_admin_learning_material_path(learning_material), class: 'btn btn-primary btn-xs')} #{link_to("Download Logs", download_logs_admin_learning_material_path(learning_material), class: 'btn btn-warning btn-xs')}"
      ]
    end
  end

  def learning_materials
    @learning_materials ||= fetch_learning_materials
  end

  def fetch_learning_materials
    learning_materials = LearningMaterial.order("#{sort_column} #{sort_direction}")
    learning_materials = learning_materials.page(page).per_page(per_page)
    if params[:sSearch].present?
      learning_materials = learning_materials.where("title ilike :search
                            ", search: "%#{params[:sSearch]}%")
    end
    learning_materials.distinct
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id title id]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end
