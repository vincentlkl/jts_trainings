# config valid only for current version of Capistrano
lock '3.11.0'

set :application, 'jts_trainings'
set :repo_url, 'git@gitlab.com:vincentlkl/jts_trainings.git'
set :deploy_to, '/home/deploy/myapp'
set :linked_files, %w{config/database.yml config/app_environment_variables.rb}
set :pty, true
set :ssh_options, {:forward_agent => true}


# new update  form https://github.com/capistrano/bundler/issues/45
set :bundle_binstubs, nil
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
# set :delayed_job_pid_dir, '/tmp'

set :delayed_job_server_role, :worker
set :delayed_job_args, "-n 1"

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

set :config_backend, :dotenv # currently this is the only supported backend
set :config_file,    '.env'  # you might want to use .env.production instead

def rails_env
  fetch(:rails_env, false) ? "RAILS_ENV=#{fetch(:rails_env)}" : ''
end

after 'deploy:publishing', 'deploy:restart'

# bundle exec cap production deploy
# bundle exec cap production_malaysia deploy
namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'

end
