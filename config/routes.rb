Rails.application.routes.draw do
  devise_for :users
  match "contact" => "contacts#create", :via => :post, as: "contacts"
  match "schedule" => "pages#schedule", :via => :get, as: "schedule"
  match "about-us" => "pages#about_us", :via => :get, as: "about_us"
  match "services" => "pages#services", :via => :get, as: "services"
  match "contact" => "pages#contact", :via => :get, as: "contact"
  
  resources :learning_materials, only: %I[index show]

  #banner images
  resources :banner_images do
    match 'move_up' => 'banner_images#move_up', via: :post, as: 'move_up'
    match 'move_down' => 'banner_images#move_down', via: :post, as: 'move_down'
  end

  namespace :admin do
    resources :users do
      member do
        get :sign_in_logs
      end
    end
    resources :learning_materials do
      member do
        get :download_logs
      end

      collection do
        post :s3_upload
      end
    end
  end

  match 'admin' => 'banner_images#index', via: :get

  # programmes
  match "programmes/:id" => "programmes#index", :via => :get, as: "programmes"

  root 'pages#home'
end  