S3DirectUpload.config do |c|
  c.access_key_id = ENV['AWS_ACCESS_KEY_ID']
  c.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
  c.bucket = ENV['AWS_BUCKET']
  c.region = ENV['S3_REGION']
  c.url = "https://s3-#{ENV['S3_REGION']}.amazonaws.com/#{ENV['AWS_BUCKET']}/"
end
